//___FILEHEADER___

import XCTest
@testable import Movie_info___24i

class ___FILEBASENAMEASIDENTIFIER___: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
}

class modelTests: XCTestCase {
    
    var movies: PopularMovies?
    var videoId: String?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.movies = nil
    }
    
    func testModelGeneral() {
        DataSource().getInfo {
            self.movies = $0.self
            XCTAssert(self.movies != nil)
            XCTAssert(self.movies?.totalMovies == 10000)
        }
    }
    
    func testModelFilmPage() {
        DataSource().getFilms(page: 2) {
            self.movies = $0.self
            XCTAssert(self.movies != nil)
            XCTAssert(self.movies?.lastPage == 2)
        }
    }
    
    func testModelVideo() {
        DataSource().getVideos(id: 475557) {
            self.videoId = $0.results![0].key
            XCTAssert(self.videoId != nil)
        }
    }
    
    func testConnectivity() {
         XCTAssert(Connectivity.isConnectedToInternet)
    }

}
