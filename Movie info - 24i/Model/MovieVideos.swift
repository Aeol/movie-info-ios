//
//  MovieVideos.swift
//  Movie info - 24i
//
//  Created by Samuel Mensak on 11/11/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation

// videos for given movie
struct VideoInfo: Codable {
    let id: Int?
    let results: [Videos]?
}

struct Videos: Codable {
        let id, iso639_1, iso3166_1, key: String?
        let name, site: String?
        let size: Int?
        let type: String?

        enum CodingKeys: String, CodingKey {
            case id
            case iso639_1 = "iso_639_1"
            case iso3166_1 = "iso_3166_1"
            case key, name, site, size, type
        }
    }
