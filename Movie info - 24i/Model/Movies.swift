//
//  Movies.swift
//  Movie info - 24i
//
//  Created by Samuel Mensak on 11/11/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit


// general info + movies
struct PopularMovies: Codable {
    let lastPage: Int?
    var movies: [Movie]?
    let totalMovies, totalPages: Int?
    var loadedMovies: Int?

    enum CodingKeys: String, CodingKey {
        case lastPage = "page"
        case movies = "results"
        case totalMovies = "total_results"
        case totalPages = "total_pages"
        case loadedMovies
    }
}

// individual movie
struct Movie: Codable {
    var image: UIImage?
    let posterPath: String?
    let adult: Bool?
    let overview, releaseDate: String?
    let genreids: [Int]?
    let id: Int?
    let originalTitle, originalLanguage, title, backdropPath: String?
    let popularity: Double?
    let voteCount: Int?
    let video: Bool?
    let voteAverage: Double?

    enum CodingKeys: String, CodingKey {
        case posterPath = "poster_path"
        case adult, overview
        case releaseDate = "release_date"
        case genreids = "genre_ids"
        case id
        case originalTitle = "original_title"
        case originalLanguage = "original_language"
        case title
        case backdropPath = "backdrop_path"
        case popularity
        case voteCount = "vote_count"
        case video
        case voteAverage = "vote_average"
    }
}
