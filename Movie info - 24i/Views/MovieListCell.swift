//
//  MovieListCell.swift
//  Movie info - 24i
//
//  Created by Samuel Mensak on 11/11/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


class MovieListCell: UITableViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    
    
    func setup(title: String, image: String ) {
        movieTitle.text = title
        
        if (image != "") { // if image given download it, otherwise leave empty
            DataSource().getImage(image: image) {
                self.movieImage.image = UIImage(data: $0)
            }
        }
    }

}

