//
//  DataSource.swift
//  Movie info - 24i
//
//  Created by Samuel Mensak on 11/11/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import Alamofire

class DataSource {
    
    // get general info + !st page of movies from API
    func getInfo(completion: @escaping (PopularMovies) -> ()) {
        
        Alamofire.request("https://api.themoviedb.org/3/movie/popular?api_key=0c45e27211c256c4d49f3aa91f8e8845&").validate().responseJSON { response in

            let result = try! JSONDecoder().decode(PopularMovies.self, from: response.data!)
    
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }

    // get page from API
    func getFilms(page: Int, completion: @escaping (PopularMovies) -> ()) {
        
        Alamofire.request("https://api.themoviedb.org/3/movie/popular?api_key=0c45e27211c256c4d49f3aa91f8e8845&page=" + String(page)).validate().responseJSON { response in

            let result = try! JSONDecoder().decode(PopularMovies.self, from: response.data!)

            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    // get page no.: id from API
    func getVideos(id: Int, completion: @escaping (VideoInfo) -> ()) {
        
        let url = "https://api.themoviedb.org/3/movie/" + String(id) + "/videos?api_key=0c45e27211c256c4d49f3aa91f8e8845&language=en-US"
        Alamofire.request(url).validate().responseJSON { response in

            let result = try! JSONDecoder().decode(VideoInfo.self, from: response.data!)

            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    // get image from API
    func getImage(image: String, completion: @escaping (Data) -> ()) {
        
        let url = "https://image.tmdb.org/t/p/w342" + image
        let remoteImageURL = URL(string: url)

        Alamofire.request(remoteImageURL!).validate().responseData { (response) in
            if response.error == nil {

                if let data = response.data {
                    DispatchQueue.main.async {
                        completion(data)
                    }
                }
            }
        }
    }
    
    
}
