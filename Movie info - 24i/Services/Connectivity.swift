//
//  Connectivity.swift
//  Movie info - 24i
//
//  Created by Samuel Mensak on 13/11/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet: Bool { // network status
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    // Note: Maybe add API reachablility check?
}
