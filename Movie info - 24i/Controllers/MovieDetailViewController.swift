//
//  MovieDetailViewController.swift
//  Movie info - 24i
//
//  Created by Samuel Mensak on 11/11/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import XCDYouTubeKit
import Alamofire

class MovieDetailViewController: UITableViewController {
    
    let playerViewController = AVPlayerViewController()
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    
    var movie: Movie?
    var videoId: String?
    
    @IBAction func buttonClicked(_ sender: Any) {
    
        // open playerVC
        present(playerViewController, animated: true, completion: nil)

        // get video
        XCDYouTubeClient.default().getVideoWithIdentifier(videoId!) {
            [weak playerViewController] (video: XCDYouTubeVideo?, error: Error?) in
            if let streamURL = (video?.streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ??
                                video?.streamURLs[XCDYouTubeVideoQuality.HD720.rawValue] ??
                                video?.streamURLs[XCDYouTubeVideoQuality.medium360.rawValue] ??
                                video?.streamURLs[XCDYouTubeVideoQuality.small240.rawValue]) {
                
                // set player and autoplay
                playerViewController?.player = AVPlayer(url: streamURL)
                playerViewController?.player?.play()
                
                // assign observer for end of video playback (so player can be closed)
                NotificationCenter.default.addObserver(self,
                                                       selector: #selector(self.playerDidFinishPlaying),
                                                       name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                       object: playerViewController?.player?.currentItem)
            } else { // unable to get video
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // video finished playing, close palyer
    @objc func playerDidFinishPlaying(note: NSNotification) {
        playerViewController.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        button.layer.cornerRadius = button.frame.height / 2 // button design
        
        // data asignment
        overview.text = self.movie?.overview
        releaseDate.text = self.movie?.releaseDate!.replacingOccurrences(of: "-", with: ".")
        movieTitle.text = self.movie?.title
        movieImage.image = self.movie?.image
        
        // get list of videos
        DataSource().getVideos(id: (self.movie?.id)!) {
            self.videoId = $0.results![0].key //select 1st video
        }
        // get image
        DataSource().getImage(image: (self.movie?.backdropPath)!) {
            self.movieImage.image = UIImage(data: $0)
        }
    }
}
