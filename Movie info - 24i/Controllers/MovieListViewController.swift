//
//  MovieListViewController.swift
//  Movie info - 24i
//
//  Created by Samuel Mensak on 11/11/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


class MovieListViewController: UITableViewController, UISearchBarDelegate {
    
    var movies: PopularMovies? //model
    var lastSelectedIndexPath: IndexPath!


    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
     
    override func viewDidLoad() {
        
        self.tableView.isUserInteractionEnabled = false // disable user interaction until data are loaded
        
        if !Connectivity.isConnectedToInternet { // check for connectivity
            let alert = UIAlertController(title: "No internet", message: "Unable to get data!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Retry", style: .cancel, handler: {( action: UIAlertAction) -> Void in
                if !Connectivity.isConnectedToInternet { // retry
                    self.present(alert, animated: true)
                } else {
                    self.tableView.reloadData()
                }
            }))
            self.present(alert, animated: true)
        }
        
        DataSource().getInfo { // get data (info + 1st page)
            self.movies = $0.self
        }
        
        for n in 2...5 { // get next 4 pages
            DataSource().getFilms(page: n) {
                
                self.movies?.movies?.append(contentsOf: $0.self.movies!) // add page to model
                self.tableView.reloadData()
            }
        }
        self.tableView.tableFooterView = UIView() // set view for footer
        self.tableView.isUserInteractionEnabled = true //data loaded, enable user interaction

    
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // TODO
        // add searchBar in Main.storyboard
        // model.filetr($0 == searchText)
        // update number of table cells
        self.tableView.reloadData()
    }
    
     
     public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
     }

     
     public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100 // top 100 movies
     }
    
    public override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lastSelectedIndexPath = tableView.indexPathForSelectedRow // remember last item selected by user
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "ShowDetail", sender: self)
        }
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieListCell") as? MovieListCell
        
        if Connectivity.isConnectedToInternet { // if connection availible, let cell set up properly (eg. download data)
            let title = (self.movies?.movies?[indexPath.row].title)
            let image = (self.movies?.movies?[indexPath.row].backdropPath)
            cell!.setup(title: title ?? "", image: image ?? "")
            return cell!
        } else { // no connection, create dummy cell
            cell!.setup(title: "", image: "")
            return cell!
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" { // pass info to detail view
            let dvc = segue.destination as? MovieDetailViewController
            dvc!.movie = self.movies?.movies?[lastSelectedIndexPath.row]
        }
    }
    
}
